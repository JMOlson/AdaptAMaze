<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="FRAME_A_L" urn="urn:adsk.eagle:symbol:13882/1" library_version="1">
<frame x1="0" y1="0" x2="279.4" y2="215.9" columns="6" rows="5" layer="94" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD" urn="urn:adsk.eagle:symbol:13864/1" library_version="1">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="FRAME_A_L" urn="urn:adsk.eagle:component:13939/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt; A Size , 8 1/2 x 11 INCH, Landscape&lt;p&gt;</description>
<gates>
<gate name="G$1" symbol="FRAME_A_L" x="0" y="0" addlevel="always"/>
<gate name="G$2" symbol="DOCFIELD" x="172.72" y="0" addlevel="always"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SnapEDA-Library">
<packages>
<package name="CUI_SJ1-42514">
<wire x1="-0.75" y1="0.4" x2="0.75" y2="0.4" width="0.0001" layer="46"/>
<wire x1="0.75" y1="0.4" x2="0.75" y2="-0.4" width="0.0001" layer="46"/>
<wire x1="0.75" y1="-0.4" x2="-0.75" y2="-0.4" width="0.0001" layer="46"/>
<wire x1="-0.75" y1="-0.4" x2="-0.75" y2="0.4" width="0.0001" layer="46"/>
<wire x1="4.95" y1="1.5" x2="6.45" y2="1.5" width="0.0001" layer="46"/>
<wire x1="6.45" y1="1.5" x2="6.45" y2="0.7" width="0.0001" layer="46"/>
<wire x1="6.45" y1="0.7" x2="4.95" y2="0.7" width="0.0001" layer="46"/>
<wire x1="4.95" y1="0.7" x2="4.95" y2="1.5" width="0.0001" layer="46"/>
<wire x1="4.95" y1="8.7" x2="6.45" y2="8.7" width="0.0001" layer="46"/>
<wire x1="6.45" y1="8.7" x2="6.45" y2="7.9" width="0.0001" layer="46"/>
<wire x1="6.45" y1="7.9" x2="4.95" y2="7.9" width="0.0001" layer="46"/>
<wire x1="4.95" y1="7.9" x2="4.95" y2="8.7" width="0.0001" layer="46"/>
<wire x1="0.25" y1="9.1" x2="1.75" y2="9.1" width="0.0001" layer="46"/>
<wire x1="1.75" y1="9.1" x2="1.75" y2="8.3" width="0.0001" layer="46"/>
<wire x1="1.75" y1="8.3" x2="0.25" y2="8.3" width="0.0001" layer="46"/>
<wire x1="0.25" y1="8.3" x2="0.25" y2="9.1" width="0.0001" layer="46"/>
<wire x1="-1.8" y1="-0.8" x2="-1.8" y2="2.7" width="0.127" layer="51"/>
<wire x1="-1.8" y1="2.7" x2="-1.8" y2="6.7" width="0.127" layer="51"/>
<wire x1="-1.8" y1="6.7" x2="-1.8" y2="9.2" width="0.127" layer="51"/>
<wire x1="-1.8" y1="9.2" x2="8.2" y2="9.2" width="0.127" layer="51"/>
<wire x1="8.2" y1="9.2" x2="8.2" y2="-0.8" width="0.127" layer="51"/>
<wire x1="8.2" y1="-0.8" x2="-1.8" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-1.8" y1="-0.8" x2="-1.8" y2="2.7" width="0.127" layer="21"/>
<wire x1="-1.8" y1="2.7" x2="-1.8" y2="6.7" width="0.127" layer="21"/>
<wire x1="-1.8" y1="6.7" x2="-1.8" y2="9.2" width="0.127" layer="21"/>
<wire x1="-1.8" y1="9.2" x2="-0.5" y2="9.2" width="0.127" layer="21"/>
<wire x1="2.5" y1="9.2" x2="8.2" y2="9.2" width="0.127" layer="21"/>
<wire x1="8.2" y1="9.2" x2="8.2" y2="-0.8" width="0.127" layer="21"/>
<wire x1="8.2" y1="-0.8" x2="-1.8" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.8" y1="6.7" x2="-3.8" y2="6.7" width="0.127" layer="21"/>
<wire x1="-3.8" y1="6.7" x2="-3.8" y2="2.7" width="0.127" layer="21"/>
<wire x1="-3.8" y1="2.7" x2="-1.8" y2="2.7" width="0.127" layer="21"/>
<wire x1="-1.8" y1="2.7" x2="-3.8" y2="2.7" width="0.127" layer="51"/>
<wire x1="-3.8" y1="2.7" x2="-3.8" y2="6.7" width="0.127" layer="51"/>
<wire x1="-3.8" y1="6.7" x2="-1.8" y2="6.7" width="0.127" layer="51"/>
<wire x1="-2.05" y1="9.55" x2="8.45" y2="9.55" width="0.05" layer="39"/>
<wire x1="8.45" y1="9.55" x2="8.45" y2="-1.05" width="0.05" layer="39"/>
<wire x1="8.45" y1="-1.05" x2="-2.05" y2="-1.05" width="0.05" layer="39"/>
<wire x1="-2.05" y1="-1.05" x2="-2.05" y2="2.45" width="0.05" layer="39"/>
<wire x1="-2.05" y1="2.45" x2="-4.05" y2="2.45" width="0.05" layer="39"/>
<wire x1="-4.05" y1="2.45" x2="-4.05" y2="6.95" width="0.05" layer="39"/>
<wire x1="-4.05" y1="6.95" x2="-2.05" y2="6.95" width="0.05" layer="39"/>
<wire x1="-2.05" y1="6.95" x2="-2.05" y2="9.55" width="0.05" layer="39"/>
<circle x="-0.122" y="-1.315" radius="0.1" width="0.2" layer="21"/>
<text x="-1.25541875" y="9.671659375" size="1.42515" layer="25">&gt;NAME</text>
<text x="-1.78046875" y="-3.08581875" size="1.42278125" layer="27">&gt;VALUE</text>
<circle x="-0.467" y="1.175" radius="0.1" width="0.2" layer="51"/>
<pad name="1" x="0" y="0" drill="0.8" diameter="1.2" shape="long"/>
<pad name="2" x="5.7" y="1.1" drill="0.8" diameter="1.2" shape="long"/>
<pad name="3" x="5.7" y="8.3" drill="0.8" diameter="1.2" shape="long" rot="R180"/>
<pad name="4" x="1" y="8.7" drill="0.8" diameter="1.2" shape="long"/>
<hole x="1.2" y="4.7" drill="1.6"/>
<hole x="6.2" y="4.7" drill="1.6"/>
</package>
<package name="CUI_SJ-43514">
<wire x1="-5.5" y1="4.7" x2="5.5" y2="4.7" width="0.127" layer="51"/>
<wire x1="5.5" y1="4.7" x2="5.5" y2="-4.3" width="0.127" layer="51"/>
<wire x1="-5.5" y1="4.7" x2="-5.5" y2="3" width="0.127" layer="51"/>
<wire x1="-5.5" y1="3" x2="-5.5" y2="2.25" width="0.127" layer="51"/>
<wire x1="-5.5" y1="2.25" x2="-4.2" y2="2.25" width="0.127" layer="51"/>
<wire x1="-4.2" y1="2.25" x2="-4.2" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-4.2" y1="-2.25" x2="-5.5" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-5.5" y1="-2.25" x2="-5.5" y2="-3" width="0.127" layer="51"/>
<wire x1="-5.5" y1="-3" x2="-5.5" y2="-4.3" width="0.127" layer="51"/>
<wire x1="-5.5" y1="-4.3" x2="5.5" y2="-4.3" width="0.127" layer="51"/>
<wire x1="-5.5" y1="3" x2="-8.5" y2="3" width="0.127" layer="51"/>
<wire x1="-8.5" y1="3" x2="-8.5" y2="-3" width="0.127" layer="51"/>
<wire x1="-8.5" y1="-3" x2="-5.5" y2="-3" width="0.127" layer="51"/>
<wire x1="-3.8" y1="4.7" x2="-5.5" y2="4.7" width="0.127" layer="21"/>
<wire x1="-5.5" y1="4.7" x2="-5.5" y2="3" width="0.127" layer="21"/>
<wire x1="-5.5" y1="3" x2="-5.5" y2="2.25" width="0.127" layer="21"/>
<wire x1="-5.5" y1="2.25" x2="-4.2" y2="2.25" width="0.127" layer="21"/>
<wire x1="-4.2" y1="2.25" x2="-4.2" y2="-2.25" width="0.127" layer="21"/>
<wire x1="-4.2" y1="-2.25" x2="-5.5" y2="-2.25" width="0.127" layer="21"/>
<wire x1="-5.5" y1="-2.25" x2="-5.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-5.5" y1="-3" x2="-5.5" y2="-4.3" width="0.127" layer="21"/>
<wire x1="-5.5" y1="-4.3" x2="-3.75" y2="-4.3" width="0.127" layer="21"/>
<wire x1="-5.5" y1="-3" x2="-8.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-8.5" y1="-3" x2="-8.5" y2="3" width="0.127" layer="21"/>
<wire x1="-8.5" y1="3" x2="-5.5" y2="3" width="0.127" layer="21"/>
<wire x1="-1.4" y1="4.7" x2="5.5" y2="4.7" width="0.127" layer="21"/>
<wire x1="5.5" y1="4.7" x2="5.5" y2="2.7" width="0.127" layer="21"/>
<wire x1="5.5" y1="0.35" x2="5.5" y2="-4.3" width="0.127" layer="21"/>
<wire x1="5.5" y1="-4.3" x2="3.4" y2="-4.3" width="0.127" layer="21"/>
<wire x1="1.5" y1="-4.3" x2="-1.55" y2="-4.3" width="0.127" layer="21"/>
<wire x1="-5.5" y1="8.35" x2="-5.5" y2="2.25" width="0.0001" layer="46"/>
<wire x1="-5.5" y1="2.25" x2="-4.2" y2="2.25" width="0.0001" layer="46"/>
<wire x1="-4.2" y1="2.25" x2="-4.2" y2="-2.25" width="0.0001" layer="46"/>
<wire x1="-4.2" y1="-2.25" x2="-5.5" y2="-2.25" width="0.0001" layer="46"/>
<wire x1="-5.5" y1="-2.25" x2="-5.5" y2="-8.85" width="0.0001" layer="46"/>
<wire x1="-8.75" y1="3.25" x2="-5.75" y2="3.25" width="0.05" layer="39"/>
<wire x1="-5.75" y1="3.25" x2="-5.75" y2="5" width="0.05" layer="39"/>
<wire x1="-5.75" y1="5" x2="-3.65" y2="5" width="0.05" layer="39"/>
<wire x1="-3.65" y1="5" x2="-3.65" y2="5.85" width="0.05" layer="39"/>
<wire x1="-3.65" y1="5.85" x2="-1.5" y2="5.85" width="0.05" layer="39"/>
<wire x1="-1.5" y1="5.85" x2="-1.5" y2="5" width="0.05" layer="39"/>
<wire x1="-1.5" y1="5" x2="5.75" y2="5" width="0.05" layer="39"/>
<wire x1="5.75" y1="5" x2="5.75" y2="2.5" width="0.05" layer="39"/>
<wire x1="5.75" y1="2.5" x2="6.5" y2="2.5" width="0.05" layer="39"/>
<wire x1="6.5" y1="2.5" x2="6.5" y2="0.5" width="0.05" layer="39"/>
<wire x1="6.5" y1="0.5" x2="5.75" y2="0.5" width="0.05" layer="39"/>
<wire x1="5.75" y1="0.5" x2="5.75" y2="-4.75" width="0.05" layer="39"/>
<wire x1="5.75" y1="-4.75" x2="-1.5" y2="-4.75" width="0.05" layer="39"/>
<wire x1="-1.5" y1="-4.75" x2="-1.5" y2="-5.55" width="0.05" layer="39"/>
<wire x1="-1.5" y1="-5.55" x2="-3.75" y2="-5.55" width="0.05" layer="39"/>
<wire x1="-3.75" y1="-5.55" x2="-3.75" y2="-4.5" width="0.05" layer="39"/>
<wire x1="-3.75" y1="-4.5" x2="-5.75" y2="-4.5" width="0.05" layer="39"/>
<wire x1="-5.75" y1="-4.5" x2="-5.75" y2="-3.25" width="0.05" layer="39"/>
<wire x1="-5.75" y1="-3.25" x2="-8.75" y2="-3.25" width="0.05" layer="39"/>
<wire x1="-8.75" y1="-3.25" x2="-8.75" y2="3.25" width="0.05" layer="39"/>
<text x="-5.75331875" y="-8.5049" size="0.609953125" layer="51" rot="R90">PCB EDGE</text>
<circle x="-4" y="5.5" radius="0.2" width="0.4" layer="21"/>
<text x="-4.5056" y="6.508090625" size="0.610359375" layer="25">&gt;NAME</text>
<text x="-4.75835" y="-6.51143125" size="0.610671875" layer="27">&gt;VALUE</text>
<pad name="1" x="-2.6" y="4.8" drill="1.1" shape="square"/>
<pad name="2" x="2.4" y="-3.6" drill="1.1"/>
<pad name="3" x="5.5" y="1.6" drill="1.1"/>
<pad name="4" x="-2.6" y="-4.5" drill="1.1"/>
<hole x="-2.6" y="0" drill="1.1"/>
<hole x="2.4" y="0" drill="1.1"/>
</package>
</packages>
<symbols>
<symbol name="SJ1-42514">
<wire x1="7.62" y1="7.62" x2="-4.0005" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-4.0005" y1="7.62" x2="-4.0005" y2="3.429" width="0.1524" layer="94"/>
<wire x1="-4.0005" y1="3.429" x2="-3.1115" y2="3.429" width="0.1524" layer="94"/>
<wire x1="-3.1115" y1="3.429" x2="-3.1115" y2="-2.413" width="0.1524" layer="94"/>
<wire x1="-3.1115" y1="-2.413" x2="-4.8895" y2="-2.413" width="0.1524" layer="94"/>
<wire x1="-4.8895" y1="-2.413" x2="-4.8895" y2="3.429" width="0.1524" layer="94"/>
<wire x1="-4.8895" y1="3.429" x2="-4.0005" y2="3.429" width="0.1524" layer="94"/>
<text x="-7.62828125" y="10.6796" size="1.77993125" layer="95">&gt;NAME</text>
<text x="-7.632509375" y="-9.66785" size="1.78091875" layer="96">&gt;VALUE</text>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0.635" y1="2.54" x2="-0.3175" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-0.3175" y1="1.27" x2="-1.27" y2="2.54" width="0.1524" layer="94"/>
<wire x1="8.255" y1="-5.08" x2="1.397" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="1.397" y1="-5.08" x2="0.4445" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="0.4445" y1="-3.81" x2="-0.508" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-0.635" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="5.08" x2="-1.5875" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-1.5875" y1="3.81" x2="-2.54" y2="5.08" width="0.1524" layer="94"/>
<pin name="1" x="12.7" y="7.62" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="2" x="12.7" y="-5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="3" x="12.7" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="4" x="12.7" y="5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
</symbol>
<symbol name="SJ-43514">
<wire x1="-6.096" y1="2.159" x2="-6.096" y2="-2.413" width="0.254" layer="94"/>
<wire x1="-6.096" y1="-2.413" x2="-4.064" y2="-2.413" width="0.254" layer="94"/>
<wire x1="-4.064" y1="-2.413" x2="-4.064" y2="2.159" width="0.254" layer="94"/>
<wire x1="-4.064" y1="2.159" x2="-6.096" y2="2.159" width="0.254" layer="94"/>
<wire x1="-2.286" y1="5.08" x2="-1.016" y2="3.302" width="0.254" layer="94"/>
<wire x1="-1.016" y1="3.302" x2="0.254" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<text x="-7.621940625" y="8.38413125" size="1.77845" layer="95">&gt;NAME</text>
<text x="-7.626140625" y="-10.1682" size="1.77943125" layer="96">&gt;VALUE</text>
<wire x1="2.159" y1="-2.54" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-2.54" x2="0.889" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.889" y1="-0.762" x2="2.159" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.381" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="4.953" y1="2.54" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.413" y1="2.54" x2="3.683" y2="1.016" width="0.254" layer="94"/>
<wire x1="3.683" y1="1.016" x2="4.953" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<pin name="RING2" x="10.16" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="SLEEVE" x="10.16" y="-5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="TIP" x="10.16" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="RING1" x="10.16" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SJ1-42514" prefix="J">
<description>4 Conductor 2.5 mm Audio Jack</description>
<gates>
<gate name="G$1" symbol="SJ1-42514" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CUI_SJ1-42514">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER" value="CUI Inc"/>
<attribute name="PART_REV" value="1.01"/>
<attribute name="STANDARD" value="Manufacturer Recommendation"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SJ-43514" prefix="J">
<description>SJ Series 3.5 mm 16 V 4 Conductor SMT Mid Mount Stereo Audio Jack</description>
<gates>
<gate name="G$1" symbol="SJ-43514" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CUI_SJ-43514">
<connects>
<connect gate="G$1" pin="RING1" pad="3"/>
<connect gate="G$1" pin="RING2" pad="4"/>
<connect gate="G$1" pin="SLEEVE" pad="1"/>
<connect gate="G$1" pin="TIP" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER" value="CUI INC"/>
<attribute name="PART_REV" value="1.04 "/>
<attribute name="STANDARD" value="MANUFACTURER RECOMMENDATIONS"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-molex" urn="urn:adsk.eagle:library:165">
<description>&lt;b&gt;Molex Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="53047-08" urn="urn:adsk.eagle:footprint:8078146/1" library_version="5">
<description>&lt;b&gt;1.25mm Pitch PicoBlade™ Wire-to-Board Header, Vertical, with Friction Lock, 8 Circuits&lt;/b&gt;&lt;p&gt;&lt;a href =http://www.molex.com/pdm_docs/sd/530470810_sd.pdf&gt;Datasheet &lt;/a&gt;</description>
<wire x1="-5.775" y1="-1.5" x2="5.775" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="5.775" y1="-1.5" x2="5.775" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5.775" y1="1.5" x2="-5.775" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-5.775" y1="1.5" x2="-5.775" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="-0.25" x2="-5.375" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="-5.375" y1="-0.25" x2="-5.375" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="5.375" y1="-0.25" x2="5.75" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="5.375" y1="-0.25" x2="5.375" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="-5.75" y1="0.375" x2="-5.375" y2="0.375" width="0.0508" layer="21"/>
<wire x1="5.375" y1="0.375" x2="5.75" y2="0.375" width="0.0508" layer="21"/>
<wire x1="-5.375" y1="0.375" x2="-5.375" y2="1.125" width="0.0508" layer="21"/>
<wire x1="-5.375" y1="1.125" x2="5.375" y2="1.125" width="0.0508" layer="21"/>
<wire x1="5.375" y1="1.125" x2="5.375" y2="0.375" width="0.0508" layer="21"/>
<wire x1="5.375" y1="1.125" x2="5.625" y2="1.375" width="0.0508" layer="21"/>
<wire x1="-5.375" y1="1.125" x2="-5.625" y2="1.375" width="0.0508" layer="21"/>
<pad name="1" x="4.375" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="2" x="3.125" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="3" x="1.875" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="4" x="0.625" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="5" x="-0.625" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="6" x="-1.875" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="7" x="-3.125" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="8" x="-4.375" y="0.45" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<text x="-5.625" y="1.75" size="1.27" layer="25">&gt;NAME</text>
<text x="1.125" y="1.75" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.75" y1="-1.5" x2="5.75" y2="-1.125" layer="21"/>
</package>
<package name="53048-08" urn="urn:adsk.eagle:footprint:8078188/1" library_version="5">
<description>&lt;b&gt;1.25mm Pitch PicoBlade™ Header, Right Angle, 8 Circuits&lt;/b&gt;&lt;p&gt;&lt;a href =http://www.molex.com/pdm_docs/sd/530480810_sd.pdf&gt;Datasheet &lt;/a&gt;</description>
<wire x1="-5.775" y1="-2.25" x2="-5.25" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.25" x2="5.775" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.775" y1="-2.25" x2="5.775" y2="3.125" width="0.2032" layer="21"/>
<wire x1="5.775" y1="3.125" x2="5.625" y2="3.125" width="0.2032" layer="21"/>
<wire x1="5.625" y1="3.125" x2="-5.625" y2="3.125" width="0.2032" layer="21"/>
<wire x1="-5.625" y1="3.125" x2="-5.775" y2="3.125" width="0.2032" layer="21"/>
<wire x1="-5.775" y1="3.125" x2="-5.775" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="1.5" x2="-5.25" y2="1.5" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="1.5" x2="-5" y2="1.5" width="0.0508" layer="21"/>
<wire x1="-5" y1="1.5" x2="-5" y2="0.625" width="0.0508" layer="21"/>
<wire x1="-5" y1="0.625" x2="5" y2="0.625" width="0.0508" layer="21"/>
<wire x1="5" y1="1.5" x2="5.25" y2="1.5" width="0.0508" layer="21"/>
<wire x1="5.25" y1="1.5" x2="5.75" y2="1.5" width="0.0508" layer="21"/>
<wire x1="5" y1="1.5" x2="5" y2="0.625" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="-1" x2="5.25" y2="-1" width="0.2032" layer="51"/>
<wire x1="-4.625" y1="-1.5" x2="-4.75" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-3.375" y1="-1.5" x2="-3.5" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-1.625" x2="-4" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-4.125" y1="-1.5" x2="-4" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-2.125" y1="-1.5" x2="-2.25" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.625" x2="-2.75" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-2.875" y1="-1.5" x2="-2.75" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-0.875" y1="-1.5" x2="-1" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.625" x2="-1.5" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-1.625" y1="-1.5" x2="-1.5" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="0.375" y1="-1.5" x2="0.25" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="0.25" y1="-1.625" x2="-0.25" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-0.375" y1="-1.5" x2="-0.25" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="1.625" y1="-1.5" x2="1.5" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-1.625" x2="1" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="0.875" y1="-1.5" x2="1" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="2.875" y1="-1.5" x2="2.75" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="2.75" y1="-1.625" x2="2.25" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="2.125" y1="-1.5" x2="2.25" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="4.125" y1="-1.5" x2="4" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="4" y1="-1.625" x2="3.5" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="3.375" y1="-1.5" x2="3.5" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-1.625" x2="4.75" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="4.625" y1="-1.5" x2="4.75" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-4.75" y1="-1.625" x2="-5.25" y2="-1.625" width="0.2032" layer="51"/>
<wire x1="-5.75" y1="-1" x2="-5.25" y2="-1" width="0.2032" layer="21"/>
<wire x1="-5.25" y1="-1" x2="-5.25" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-1" x2="5.75" y2="-1" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-1" x2="5.25" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-5.25" y1="1.5" x2="-5.25" y2="2.75" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="2.75" x2="5.25" y2="2.75" width="0.0508" layer="21"/>
<wire x1="5.25" y1="2.75" x2="5.25" y2="1.5" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="2.75" x2="-5.625" y2="3.125" width="0.0508" layer="21"/>
<wire x1="5.25" y1="2.75" x2="5.625" y2="3.125" width="0.0508" layer="21"/>
<wire x1="-4.5" y1="1.5" x2="-4.375" y2="2" width="0.2032" layer="21"/>
<wire x1="-4.375" y1="2" x2="-4.25" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-3.25" y1="1.5" x2="-3.125" y2="2" width="0.2032" layer="21"/>
<wire x1="-3.125" y1="2" x2="-3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.5" x2="-1.875" y2="2" width="0.2032" layer="21"/>
<wire x1="-1.875" y1="2" x2="-1.75" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-0.75" y1="1.5" x2="-0.625" y2="2" width="0.2032" layer="21"/>
<wire x1="-0.625" y1="2" x2="-0.5" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.5" y1="1.5" x2="0.625" y2="2" width="0.2032" layer="21"/>
<wire x1="0.625" y1="2" x2="0.75" y2="1.5" width="0.2032" layer="21"/>
<wire x1="1.75" y1="1.5" x2="1.875" y2="2" width="0.2032" layer="21"/>
<wire x1="1.875" y1="2" x2="2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="3" y1="1.5" x2="3.125" y2="2" width="0.2032" layer="21"/>
<wire x1="3.125" y1="2" x2="3.25" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4.25" y1="1.5" x2="4.375" y2="2" width="0.2032" layer="21"/>
<wire x1="4.375" y1="2" x2="4.5" y2="1.5" width="0.2032" layer="21"/>
<pad name="1" x="4.375" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="2" x="3.125" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="3" x="1.875" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="4" x="0.625" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="5" x="-0.625" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="6" x="-1.875" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="7" x="-3.125" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<pad name="8" x="-4.375" y="-1.25" drill="0.5" diameter="0.6984" shape="long" rot="R90"/>
<text x="-4.375" y="3.375" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.375" y="-3.75" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.625" y1="-1.5" x2="-4.125" y2="-1" layer="51"/>
<rectangle x1="-3.375" y1="-1.5" x2="-2.875" y2="-1" layer="51"/>
<rectangle x1="-2.125" y1="-1.5" x2="-1.625" y2="-1" layer="51"/>
<rectangle x1="-0.875" y1="-1.5" x2="-0.375" y2="-1" layer="51"/>
<rectangle x1="0.375" y1="-1.5" x2="0.875" y2="-1" layer="51"/>
<rectangle x1="1.625" y1="-1.5" x2="2.125" y2="-1" layer="51"/>
<rectangle x1="2.875" y1="-1.5" x2="3.375" y2="-1" layer="51"/>
<rectangle x1="4.125" y1="-1.5" x2="4.625" y2="-1" layer="51"/>
<rectangle x1="-4.625" y1="0.625" x2="-4.125" y2="1.5" layer="21"/>
<rectangle x1="-3.375" y1="0.625" x2="-2.875" y2="1.5" layer="21"/>
<rectangle x1="-2.125" y1="0.625" x2="-1.625" y2="1.5" layer="21"/>
<rectangle x1="-0.875" y1="0.625" x2="-0.375" y2="1.5" layer="21"/>
<rectangle x1="0.375" y1="0.625" x2="0.875" y2="1.5" layer="21"/>
<rectangle x1="1.625" y1="0.625" x2="2.125" y2="1.5" layer="21"/>
<rectangle x1="2.875" y1="0.625" x2="3.375" y2="1.5" layer="21"/>
<rectangle x1="4.125" y1="0.625" x2="4.625" y2="1.5" layer="21"/>
</package>
<package name="53261-08" urn="urn:adsk.eagle:footprint:8078161/1" library_version="5">
<description>&lt;b&gt;1.25mm Pitch PicoBlade™ Header, Surface Mount, Right Angle, 8 Circuits&lt;/b&gt;&lt;p&gt;&lt;a href =http://www.molex.com/pdm_docs/sd/532610871_sd.pdf&gt;Datasheet &lt;/a&gt;</description>
<wire x1="-5.775" y1="-1.375" x2="-5.25" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="-5.25" y1="-1.375" x2="5.25" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-1.375" x2="5.775" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="5.775" y1="-1.375" x2="5.775" y2="2.625" width="0.2032" layer="21"/>
<wire x1="5.775" y1="2.625" x2="5.625" y2="2.625" width="0.2032" layer="21"/>
<wire x1="5.625" y1="2.625" x2="-5.625" y2="2.625" width="0.2032" layer="21"/>
<wire x1="-5.625" y1="2.625" x2="-5.775" y2="2.625" width="0.2032" layer="21"/>
<wire x1="-5.775" y1="2.625" x2="-5.775" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="1.625" x2="-5.25" y2="1.625" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="1.625" x2="-5" y2="1.625" width="0.0508" layer="21"/>
<wire x1="-5" y1="1.625" x2="-5" y2="1" width="0.0508" layer="21"/>
<wire x1="-5" y1="1" x2="5" y2="1" width="0.0508" layer="21"/>
<wire x1="5" y1="1.625" x2="5.25" y2="1.625" width="0.0508" layer="21"/>
<wire x1="5.25" y1="1.625" x2="5.75" y2="1.625" width="0.0508" layer="21"/>
<wire x1="5" y1="1.625" x2="5" y2="1" width="0.0508" layer="21"/>
<wire x1="-5.75" y1="-0.75" x2="-5.25" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="-0.75" x2="-5.25" y2="-1.375" width="0.0508" layer="21"/>
<wire x1="5.25" y1="-0.75" x2="5.75" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="5.25" y1="-0.75" x2="5.25" y2="-1.375" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="1.625" x2="-5.25" y2="2.25" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="2.25" x2="5.25" y2="2.25" width="0.0508" layer="21"/>
<wire x1="5.25" y1="2.25" x2="5.25" y2="1.625" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="2.25" x2="-5.625" y2="2.625" width="0.0508" layer="21"/>
<wire x1="5.25" y1="2.25" x2="5.625" y2="2.625" width="0.0508" layer="21"/>
<wire x1="-4.625" y1="-1.25" x2="-4.625" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-4.625" y1="-0.75" x2="-4.125" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-4.125" y1="-0.75" x2="-4.125" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="-3.375" y1="-1.25" x2="-3.375" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-3.375" y1="-0.75" x2="-2.875" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-2.875" y1="-0.75" x2="-2.875" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="-1.25" x2="-2.125" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-2.125" y1="-0.75" x2="-1.625" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-1.625" y1="-0.75" x2="-1.625" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="-0.875" y1="-1.25" x2="-0.875" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-0.875" y1="-0.75" x2="-0.375" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="-0.375" y1="-0.75" x2="-0.375" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="0.375" y1="-1.25" x2="0.375" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="0.375" y1="-0.75" x2="0.875" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="0.875" y1="-0.75" x2="0.875" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="1.625" y1="-1.25" x2="1.625" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="1.625" y1="-0.75" x2="2.125" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="2.125" y1="-0.75" x2="2.125" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="2.875" y1="-1.25" x2="2.875" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="2.875" y1="-0.75" x2="3.375" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="3.375" y1="-0.75" x2="3.375" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="4.125" y1="-1.25" x2="4.125" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="4.125" y1="-0.75" x2="4.625" y2="-0.75" width="0.0508" layer="21"/>
<wire x1="4.625" y1="-0.75" x2="4.625" y2="-1.25" width="0.0508" layer="21"/>
<wire x1="-5.875" y1="2.25" x2="-7.75" y2="2.25" width="0.2032" layer="51"/>
<wire x1="-7.75" y1="2.25" x2="-7.75" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="-7.75" y1="-0.75" x2="-5.875" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="5.875" y1="-0.75" x2="7.75" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="7.75" y1="-0.75" x2="7.75" y2="2.25" width="0.2032" layer="51"/>
<wire x1="7.75" y1="2.25" x2="5.875" y2="2.25" width="0.2032" layer="51"/>
<smd name="1" x="4.375" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="2" x="3.125" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="3" x="1.875" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="4" x="0.625" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="5" x="-0.625" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="6" x="-1.875" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="7" x="-3.125" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="8" x="-4.375" y="-2.5" dx="0.8" dy="2" layer="1"/>
<smd name="S1" x="6.875" y="0.625" dx="2.1" dy="3" layer="1"/>
<smd name="S2" x="-6.875" y="0.625" dx="2.1" dy="3" layer="1"/>
<text x="-5.625" y="2.875" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.25" y="-0.5" size="1.27" layer="27">&gt;VALUE</text>
<text x="6.25" y="-0.25" size="1.9304" layer="51">1</text>
<rectangle x1="-4.625" y1="1" x2="-4.125" y2="1.875" layer="21"/>
<rectangle x1="-3.375" y1="1" x2="-2.875" y2="1.875" layer="21"/>
<rectangle x1="-2.125" y1="1" x2="-1.625" y2="1.875" layer="21"/>
<rectangle x1="-0.875" y1="1" x2="-0.375" y2="1.875" layer="21"/>
<rectangle x1="0.375" y1="1" x2="0.875" y2="1.875" layer="21"/>
<rectangle x1="1.625" y1="1" x2="2.125" y2="1.875" layer="21"/>
<rectangle x1="2.875" y1="1" x2="3.375" y2="1.875" layer="21"/>
<rectangle x1="4.125" y1="1" x2="4.625" y2="1.875" layer="21"/>
</package>
<package name="53398-08" urn="urn:adsk.eagle:footprint:8078175/1" library_version="5">
<description>&lt;b&gt;1.25mm Pitch PicoBlade™ Header, Surface Mount, Vertical, 8 Circuits&lt;/b&gt;&lt;p&gt;&lt;a href =http://www.molex.com/pdm_docs/sd/533980871_sd.pdf&gt;Datasheet &lt;/a&gt;</description>
<wire x1="-5.775" y1="-1.375" x2="-5" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="-5" y1="-1.375" x2="5" y2="-1.375" width="0.0508" layer="21"/>
<wire x1="5" y1="-1.375" x2="5.775" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="5.775" y1="-1.375" x2="5.775" y2="2.125" width="0.2032" layer="21"/>
<wire x1="5.775" y1="2.125" x2="5.625" y2="2.125" width="0.2032" layer="21"/>
<wire x1="5.625" y1="2.125" x2="-5.625" y2="2.125" width="0.2032" layer="21"/>
<wire x1="-5.625" y1="2.125" x2="-5.775" y2="2.125" width="0.2032" layer="21"/>
<wire x1="-5.775" y1="2.125" x2="-5.775" y2="-1.375" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="1" x2="-5.25" y2="1" width="0.0508" layer="21"/>
<wire x1="5.25" y1="1" x2="5.75" y2="1" width="0.0508" layer="21"/>
<wire x1="-5.75" y1="-0.25" x2="-5.25" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="-0.25" x2="-5.25" y2="-1" width="0.0508" layer="21"/>
<wire x1="5.25" y1="-0.25" x2="5.75" y2="-0.25" width="0.0508" layer="21"/>
<wire x1="5.25" y1="-0.25" x2="5.25" y2="-1" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="1" x2="-5.25" y2="1.75" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="1.75" x2="5.25" y2="1.75" width="0.0508" layer="21"/>
<wire x1="5.25" y1="1.75" x2="5.25" y2="1" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="1.75" x2="-5.625" y2="2.125" width="0.0508" layer="21"/>
<wire x1="5.25" y1="1.75" x2="5.625" y2="2.125" width="0.0508" layer="21"/>
<wire x1="-5.875" y1="2.125" x2="-7.75" y2="2.125" width="0.2032" layer="51"/>
<wire x1="-7.75" y1="2.125" x2="-7.75" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="-7.75" y1="-0.75" x2="-5.875" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="5.875" y1="-0.75" x2="7.75" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="7.75" y1="-0.75" x2="7.75" y2="2.125" width="0.2032" layer="51"/>
<wire x1="7.75" y1="2.125" x2="5.875" y2="2.125" width="0.2032" layer="51"/>
<wire x1="-5.75" y1="-1" x2="-5.25" y2="-1" width="0.0508" layer="21"/>
<wire x1="-5.25" y1="-1" x2="-5" y2="-1" width="0.0508" layer="21"/>
<wire x1="-5" y1="-1" x2="-5" y2="-1.375" width="0.0508" layer="21"/>
<wire x1="5.25" y1="-1" x2="5.75" y2="-1" width="0.0508" layer="21"/>
<wire x1="5" y1="-1" x2="5.25" y2="-1" width="0.0508" layer="21"/>
<wire x1="5" y1="-1" x2="5" y2="-1.375" width="0.0508" layer="21"/>
<smd name="1" x="4.375" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="2" x="3.125" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="3" x="1.875" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="4" x="0.625" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="5" x="-0.625" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="6" x="-1.875" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="7" x="-3.125" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="8" x="-4.375" y="-2.5" dx="0.8" dy="1.8" layer="1"/>
<smd name="S1" x="6.875" y="0.625" dx="2.1" dy="3" layer="1"/>
<smd name="S2" x="-6.875" y="0.625" dx="2.1" dy="3" layer="1"/>
<text x="-5.5" y="2.375" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-1" size="1.27" layer="27">&gt;VALUE</text>
<text x="6.25" y="-0.25" size="1.9304" layer="51">1</text>
<rectangle x1="-4.625" y1="0.375" x2="-4.125" y2="1" layer="21"/>
<rectangle x1="-3.375" y1="0.375" x2="-2.875" y2="1" layer="21"/>
<rectangle x1="-2.125" y1="0.375" x2="-1.625" y2="1" layer="21"/>
<rectangle x1="-0.875" y1="0.375" x2="-0.375" y2="1" layer="21"/>
<rectangle x1="0.375" y1="0.375" x2="0.875" y2="1" layer="21"/>
<rectangle x1="1.625" y1="0.375" x2="2.125" y2="1" layer="21"/>
<rectangle x1="2.875" y1="0.375" x2="3.375" y2="1" layer="21"/>
<rectangle x1="4.125" y1="0.375" x2="4.625" y2="1" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="53047-08" urn="urn:adsk.eagle:package:8078517/1" type="box" library_version="5">
<description>&lt;b&gt;1.25mm Pitch PicoBlade™ Wire-to-Board Header, Vertical, with Friction Lock, 8 Circuits&lt;/b&gt;&lt;p&gt;&lt;a href =http://www.molex.com/pdm_docs/sd/530470810_sd.pdf&gt;Datasheet &lt;/a&gt;</description>
<packageinstances>
<packageinstance name="53047-08"/>
</packageinstances>
</package3d>
<package3d name="53048-08" urn="urn:adsk.eagle:package:8078560/1" type="box" library_version="5">
<description>&lt;b&gt;1.25mm Pitch PicoBlade™ Header, Right Angle, 8 Circuits&lt;/b&gt;&lt;p&gt;&lt;a href =http://www.molex.com/pdm_docs/sd/530480810_sd.pdf&gt;Datasheet &lt;/a&gt;</description>
<packageinstances>
<packageinstance name="53048-08"/>
</packageinstances>
</package3d>
<package3d name="53261-08" urn="urn:adsk.eagle:package:8078502/1" type="box" library_version="5">
<description>&lt;b&gt;1.25mm Pitch PicoBlade™ Header, Surface Mount, Right Angle, 8 Circuits&lt;/b&gt;&lt;p&gt;&lt;a href =http://www.molex.com/pdm_docs/sd/532610871_sd.pdf&gt;Datasheet &lt;/a&gt;</description>
<packageinstances>
<packageinstance name="53261-08"/>
</packageinstances>
</package3d>
<package3d name="53398-08" urn="urn:adsk.eagle:package:8078545/1" type="box" library_version="5">
<description>&lt;b&gt;1.25mm Pitch PicoBlade™ Header, Surface Mount, Vertical, 8 Circuits&lt;/b&gt;&lt;p&gt;&lt;a href =http://www.molex.com/pdm_docs/sd/533980871_sd.pdf&gt;Datasheet &lt;/a&gt;</description>
<packageinstances>
<packageinstance name="53398-08"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MV" urn="urn:adsk.eagle:symbol:6783/2" library_version="5">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="M" urn="urn:adsk.eagle:symbol:6785/2" library_version="5">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="53?-08" urn="urn:adsk.eagle:component:8078950/3" prefix="X" library_version="5">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;
wire to board 1.25 mm (.049 inch) pitch header</description>
<gates>
<gate name="-1" symbol="MV" x="0" y="7.62" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="0" y="5.08" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="0" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="0" y="0" addlevel="always" swaplevel="1"/>
<gate name="-5" symbol="M" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-6" symbol="M" x="0" y="-5.08" addlevel="always" swaplevel="1"/>
<gate name="-7" symbol="M" x="0" y="-7.62" addlevel="always" swaplevel="1"/>
<gate name="-8" symbol="M" x="0" y="-10.16" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="047" package="53047-08">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8078517/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="MOLEX" constant="no"/>
<attribute name="MPN" value="53047-0810" constant="no"/>
<attribute name="OC_FARNELL" value="1012258" constant="no"/>
<attribute name="OC_NEWARK" value="98K9826" constant="no"/>
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="048" package="53048-08">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8078560/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="WALDOM/MOLEX" constant="no"/>
<attribute name="MPN" value="53048-0810" constant="no"/>
<attribute name="OC_FARNELL" value="9733094" constant="no"/>
<attribute name="OC_NEWARK" value="38C9913" constant="no"/>
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="261" package="53261-08">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8078502/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="MOLEX" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1125376" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="398" package="53398-08">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8078545/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="MOLEX" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1125371" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.3048" drill="0.508">
<clearance class="0" value="0.254"/>
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="FRAME_A_L" device=""/>
<part name="2.5MM_JACK" library="SnapEDA-Library" deviceset="SJ1-42514" device=""/>
<part name="3.5MM_JACK" library="SnapEDA-Library" deviceset="SJ-43514" device=""/>
<part name="MOLEX" library="con-molex" library_urn="urn:adsk.eagle:library:165" deviceset="53?-08" device="047" package3d_urn="urn:adsk.eagle:package:8078517/1"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="142.24" y="-210.82" size="1.778" layer="97">SJ-43514 PIN
1 sleeve 3.5 - Gnd
2 tip 3.5 - 5V
3 ring1 3.5 - Sensor_Sig1
4 ring2 3.5 - Sensor_Sig2
SJ1-42514 PIN
1 sleeve 2.5 - Gnd
2 tip 2.5 - Unused
3 ring1 2.5 - LED1_Sig
4 ring2 2.5 - LED2_Sig</text>
<text x="104.14" y="-208.28" size="1.778" layer="97">Well Control Board Molex Wiring
1 Sensor GND
2 Sensor_Sig1
3 Sensor_Sig2
4 +5V
5 LED GND
6 LED1_Sig
7 LED2_Sig
8</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="-215.9" smashed="yes"/>
<instance part="FRAME1" gate="G$2" x="172.72" y="-215.9" smashed="yes">
<attribute name="LAST_DATE_TIME" x="185.42" y="-214.63" size="2.54" layer="94"/>
<attribute name="SHEET" x="259.08" y="-214.63" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="190.5" y="-196.85" size="2.54" layer="94"/>
</instance>
<instance part="2.5MM_JACK" gate="G$1" x="152.4" y="-165.1" smashed="yes">
<attribute name="NAME" x="144.77171875" y="-154.4204" size="1.77993125" layer="95"/>
<attribute name="VALUE" x="144.767490625" y="-174.76785" size="1.78091875" layer="96"/>
</instance>
<instance part="3.5MM_JACK" gate="G$1" x="152.4" y="-137.16" smashed="yes">
<attribute name="NAME" x="144.778059375" y="-128.77586875" size="1.77845" layer="95"/>
<attribute name="VALUE" x="144.773859375" y="-147.3282" size="1.77943125" layer="96"/>
</instance>
<instance part="MOLEX" gate="-1" x="208.28" y="-142.24" smashed="yes">
<attribute name="NAME" x="210.82" y="-143.002" size="1.524" layer="95"/>
<attribute name="VALUE" x="207.518" y="-130.683" size="1.778" layer="96"/>
</instance>
<instance part="MOLEX" gate="-2" x="208.28" y="-139.7" smashed="yes">
<attribute name="NAME" x="210.82" y="-140.462" size="1.524" layer="95"/>
</instance>
<instance part="MOLEX" gate="-3" x="208.28" y="-132.08" smashed="yes">
<attribute name="NAME" x="210.82" y="-132.842" size="1.524" layer="95"/>
</instance>
<instance part="MOLEX" gate="-4" x="208.28" y="-134.62" smashed="yes">
<attribute name="NAME" x="210.82" y="-135.382" size="1.524" layer="95"/>
</instance>
<instance part="MOLEX" gate="-5" x="208.28" y="-147.32" smashed="yes">
<attribute name="NAME" x="210.82" y="-148.082" size="1.524" layer="95"/>
</instance>
<instance part="MOLEX" gate="-6" x="208.28" y="-152.4" smashed="yes">
<attribute name="NAME" x="210.82" y="-153.162" size="1.524" layer="95"/>
</instance>
<instance part="MOLEX" gate="-7" x="208.28" y="-149.86" smashed="yes">
<attribute name="NAME" x="210.82" y="-150.622" size="1.524" layer="95"/>
</instance>
<instance part="MOLEX" gate="-8" x="208.28" y="-154.94" smashed="yes">
<attribute name="NAME" x="210.82" y="-155.702" size="1.524" layer="95"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="+5V" class="0">
<segment>
<wire x1="162.56" y1="-134.62" x2="205.74" y2="-134.62" width="0.1524" layer="91"/>
<label x="187.96" y="-134.62" size="1.778" layer="95"/>
<pinref part="3.5MM_JACK" gate="G$1" pin="TIP"/>
<pinref part="MOLEX" gate="-4" pin="S"/>
</segment>
</net>
<net name="SENSOR_SIG1" class="0">
<segment>
<wire x1="162.56" y1="-139.7" x2="205.74" y2="-139.7" width="0.1524" layer="91"/>
<label x="187.96" y="-139.7" size="1.778" layer="95"/>
<pinref part="3.5MM_JACK" gate="G$1" pin="RING1"/>
<pinref part="MOLEX" gate="-2" pin="S"/>
</segment>
</net>
<net name="SENSOR_SIG2" class="0">
<segment>
<wire x1="162.56" y1="-132.08" x2="205.74" y2="-132.08" width="0.1524" layer="91"/>
<label x="187.96" y="-132.08" size="1.778" layer="95"/>
<pinref part="3.5MM_JACK" gate="G$1" pin="RING2"/>
<pinref part="MOLEX" gate="-3" pin="S"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="205.74" y1="-147.32" x2="180.34" y2="-147.32" width="0.1524" layer="91"/>
<wire x1="180.34" y1="-147.32" x2="180.34" y2="-157.48" width="0.1524" layer="91"/>
<wire x1="180.34" y1="-157.48" x2="165.1" y2="-157.48" width="0.1524" layer="91"/>
<label x="187.96" y="-147.32" size="1.778" layer="95"/>
<pinref part="2.5MM_JACK" gate="G$1" pin="1"/>
<pinref part="MOLEX" gate="-5" pin="S"/>
</segment>
<segment>
<wire x1="162.56" y1="-142.24" x2="205.74" y2="-142.24" width="0.1524" layer="91"/>
<label x="187.96" y="-142.24" size="1.778" layer="95"/>
<pinref part="3.5MM_JACK" gate="G$1" pin="SLEEVE"/>
<pinref part="MOLEX" gate="-1" pin="S"/>
</segment>
</net>
<net name="LED2_SIG" class="0">
<segment>
<wire x1="165.1" y1="-160.02" x2="182.88" y2="-160.02" width="0.1524" layer="91"/>
<wire x1="182.88" y1="-160.02" x2="182.88" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="182.88" y1="-149.86" x2="205.74" y2="-149.86" width="0.1524" layer="91"/>
<label x="187.96" y="-149.86" size="1.778" layer="95"/>
<pinref part="2.5MM_JACK" gate="G$1" pin="4"/>
<pinref part="MOLEX" gate="-7" pin="S"/>
</segment>
</net>
<net name="LED1_SIG" class="0">
<segment>
<pinref part="2.5MM_JACK" gate="G$1" pin="3"/>
<wire x1="165.1" y1="-162.56" x2="185.42" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-162.56" x2="185.42" y2="-152.4" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-152.4" x2="205.74" y2="-152.4" width="0.1524" layer="91"/>
<pinref part="MOLEX" gate="-6" pin="S"/>
<label x="187.96" y="-152.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="2.5MM_JACK" gate="G$1" pin="2"/>
<wire x1="165.1" y1="-170.18" x2="187.96" y2="-170.18" width="0.1524" layer="91"/>
<wire x1="187.96" y1="-170.18" x2="187.96" y2="-154.94" width="0.1524" layer="91"/>
<wire x1="187.96" y1="-154.94" x2="205.74" y2="-154.94" width="0.1524" layer="91"/>
<pinref part="MOLEX" gate="-8" pin="S"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="113,1,139.596,-108.054,FRAME1,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
