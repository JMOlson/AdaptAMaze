# AdaptAMaze Track Joint

Track joints hold track pieces together. Specialized track joints also hold barriers.

Track joints are 3D printed. We recommend printing with a filament printer in PLA.

Track joints need two nuts and two screws to secure the track pieces. See Parts List for details.

## Track Joint Nomenclature

### General
- **Main**: Track joint part that spans the width of tracks. Needs two nuts to be inserted.
- **Side**: Track joint part that screws into Main to secure the track joint and two track pieces.

### Basic Track Joint
Standard track joint that can join any track pieces. Produces a width of 0.25" between track pieces.

### Pneumatic Barriers and Track Joint
Track joint that also holds a barrier and the barrier's air cylinder. The air cylinder's nut is held in place by the Main and the cylinder is screwed in to secure the cylinder. A groove is cut for the barrier to move up and down.

Pneumatic barrier track joints result in a wider gap between track pieces to allow for the barrier and barrier fastener. As such, the 18" standard grid will be lost. To avoid this, you can use barrier track joints throughout the maze.

- **Bar Rod Fastener**: Part that attaches to the threaded rod in the air cylinder and uses two screws to hold the barrier. Needs brass threaded inserts. See Parts List for details.

## Provided Files
- STL
- gcode
- STEP
- SLDPRT & SLDASM